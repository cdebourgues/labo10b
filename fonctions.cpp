/* 
 \file fonction.cpp
 \author Cyril & Julia
 \brief Laboratoire 10b
 \date January 18, 2016, 7:37 PM
 */
#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;


/**
\brief fonction qui retourne le premier jour de l'annee
\param[in] annee l'annee du calendrier à imprimer
\return[out]Cette fonction retourne un chiffre entre 0 et 6 0 = Samedi 6 = vendredi
 * formule trouver sur http://mathforum.org/library/drmath/view/55837.html
 */

// Variable "jour" pas utilisée ??
int connaitreJour( int annee )
{
    int annee1 = annee - 1;
    int N = 1 + 2 * 13 + ( 3 * ( 13 + 1 ) / 5 ) + annee1 + ( annee1 / 4 ) - ( annee1 / 100 ) + ( annee1 / 400) + 2;
    int jour = N % 7;
    return N;
}

void imprimer( int annee, int premierJour, int jourSemaine, bool bi )
{

}

/**
 \brief fonction qui détermine si une année est bissextile ou non
 \param[in] annee l'annee à déterminer
 \return[out] retourne true si l'année est bissextile, false si elle ne l'est pas
 */
bool estBissextile( int annee )
{
    if ( ( annee % 400 == 0 ) || ( annee % 4 == 0 && annee % 100 != 0 ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
 \brief fonction qui détermine le nombre de jours dans un mois donné
 \param[in] mois le numero du mois, par exemple 1 pour janvier ou 7 pour juillet
 \return[out] retourne le nombres de jour pour le mois concerné, par exemple 28, 30 ou 31
 \
 \Eventuellement à améliorer : passer en paramètre l'année pour pouvoir prendre en compte les années bissextiles et ajouter +1 au mois de février si besoin
 */
int nbrJourParMois( int annee, int mois )
{
    unsigned int nbrJours;
    
    if ( ( mois <= 6 && mois % 2 != 0 ) or ( mois > 6 && mois % 2 == 0 ) )
    {
        nbrJours = 31;
    }
    else if ( mois == 2 )
    {
        nbrJours = 28;
        if ( estBissextile( annee ) )
        {
            nbrJours = 29;
        }
    }
    else
    {
        nbrJours = 30;
    }
    
    return nbrJours;
}

/**
 \brief Fonction de validation d'un entier
 \param[in] invite une chaîne de caractère correspondant au premier message affiché à l'utilsateur
 \param[in] errorMsg une chaîne de caractère correspondant au message d'erreur en cas de saisie erronée
 \param[in] borneDebut un entier définissant la valeur minimum acceptée pour la saisie ( la valeur est inclus )
 \param[in] borneFin un entier définissant la valeur maxmium acceptée pour la saisie ( la valeur est inclus )
 \param[in,out] valeur un entier saisie au clavier qui sera testé
 \return la valeur entière saisie si valide
 
 Cette fonction se charge de valider un entier saisie au clavier par l'utilisateur. La fonction renvoi la valeur saisie dans le cas où cette dernière est comprise dans les bornes passées en paramètres. En cas d'erreur ou de saisie hors des bornes, la fonction boucle sur un message d'erreur et une nouvelle invite.
 */
int validerEntier( string invite, string errorMsg, int borneDebut, int borneFin, int& entier )
{
    // Saisie non-valide par défaut
    bool valid = false;
    do
    {
        cout << invite;
        cin >> entier;
        
        if ( cin.good() && entier <= borneFin && entier >= borneDebut )
        {
            // Saisie OK
            valid = true;
        }
        else
        {
            // Reset et vidage du buffer
            cin.clear();
            cin.ignore ( std::numeric_limits<std::streamsize>::max(), '\n' );
            cout << errorMsg << endl;
        }
    } while ( !valid );
    return entier;
}