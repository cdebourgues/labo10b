/* 
 \file  main.cpp
 \author Cyril & Julia
 \brief Laboratoire 10b
 \date 18, 2016, 7:37 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include "functions.hpp"
using namespace std;

/*! \mainpage Laboratoire 10
 *
 * \section Objectifs
 *  Imprimer un calendrier annuelle, en laissant l'utilisateur choisir l'annee et
 * quel jour le calendrier utilise comme référence.S
 */

const unsigned int ANNEE_MIN = 1600;
const unsigned int ANNEE_MAX = 3000;

int main()
{
    // Variables
    int annee;

    string invite = "Quelle annee voulez-vous afficher? (1600-3000) : ";
    string errorMsg = "Saisie erronée ! Veuillez saisir un entier compris entre 1600 et 3000 !";
    annee = validerEntier( invite, errorMsg, ANNEE_MIN, ANNEE_MAX, annee );
    
    cout << "Cette année comporte " << connaitreJour( annee ) << " jours" << endl;
    for ( int i = 1; i <= 12; i++ )
    {
        cout << i << " => " << nbrJourParMois( annee, i ) << endl;
    }

    
//    Pour les tests
//    while( cin >> annee )
//    {
//        cout << annee << endl;
//        cout << "Cette année comporte " << connaitreJour( annee ) << " jours" << endl;
//
//        // Itérer chaque mois
//        for ( int i = 1; i <= 12; i++ )
//        {
//            cout << i << " => " << nbrJourParMois( annee, i ) << endl;
//        }
//        cout << endl;
//    }
    
    return 0;
}

