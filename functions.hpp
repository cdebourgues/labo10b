/* 
 \file  functions.hpp
 \author Cyril & Julia
 \brief Laboratoire 10b
 \date January 18, 2016, 8:44 PM
 */

#ifndef FUNCTIONS_HPP
#define	FUNCTIONS_HPP
#include <iostream>
#include <cstdlib>
#include <iomanip>
using namespace std;

#endif	/* FUNCTIONS_HPP */

void imprimer( int annee, int premierJour, int jourSemaine );

int connaitreJour( int annee );

// Nom de fonction plus clair
int nbrJourParMois( int annee, int mois );

// Effectivement
bool estBissextile( int annee );

int validerEntier( string invite, string errorMsg, int borneDebut, int borneFin, int& entier );